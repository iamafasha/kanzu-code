<?php

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";


function extractEmails($text) {
    $pattern = '/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}/';
    preg_match_all($pattern, $text, $matches);
    return $matches[0];
}

function extractPhoneNumbers($text) {
    $pattern = '/\b\d{10,15}\b/';
    preg_match_all($pattern, $text, $matches);
    return $matches[0];
}

function extractUrls($text) {
    $pattern = '/https?:\/\/[^\s]+/';
    preg_match_all($pattern, $text, $matches);
    return $matches[0];
}

$emailAddresses = extractEmails($paragraph);
$phoneNumbers = extractPhoneNumbers($paragraph);
$urls = extractUrls($paragraph);

echo "Email Addresses: " . implode(", ", $emailAddresses) . "\n";
echo "Phone Numbers: " . implode(", ", $phoneNumbers) . "\n";
echo "URLs: " . implode(", ", $urls) . "\n";
