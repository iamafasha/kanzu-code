<?php 

function getPunctuationMarksFromFile($filename) {
    $fileContents = file_get_contents($filename);

    if ($fileContents === false) {
        return [];
    }

    $pattern = '/[[:punct:]]+/u'; 
    preg_match_all($pattern, $fileContents, $matches);

    $punctuationMarks = $matches[0];

    return $punctuationMarks;
}

$filename = 'test-file.txt'; 
$punctuationMarks = getPunctuationMarksFromFile($filename);

print_r($punctuationMarks);
