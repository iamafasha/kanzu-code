<?php


function generateFibonacci(array $numbers): array
{

    // Sort the array in descending order
    rsort($numbers);

    // Initialize variables to store Fibonacci numbers
    $fibonacci = [];
    $a = 0;
    $b = 1;

    // Generate Fibonacci numbers starting from the seventh number
    for ($i = 0; $i < 100; $i++) {
        $fibonacci[] = $a;
        $next = $a + $b;
        $a = $b;
        $b = $next;
    }

    // Slice the array to start from the seventh number
    $fibonacci = array_slice($fibonacci, 6);

    return $fibonacci;
}

$numbers = range(0, 100);

print_r(generateFibonacci($numbers));