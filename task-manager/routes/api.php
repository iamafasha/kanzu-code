<?php

use App\Http\Controllers\MilestoneController;
use App\Http\Controllers\ProjectController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

require __DIR__.'/auth.php';



Route::resource('engineers', UserController::class)->only(['show']);

Route::middleware(['auth:sanctum'])->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/projects', ProjectController::class);
Route::resource('/milestones', MilestoneController::class);

Route::middleware(['auth:sanctum', 'project_manager'])->group(function (){
    Route::post('/project/{project}/complete' , [ProjectController::class, 'completeProject']);
    Route::post('/project/{project}/add/engineer', [ProjectController::class, 'addEngineer' ]);
    Route::delete('/project/{project}/remove/{user}/engineer', [ProjectController::class, 'removeEngineer' ]);
    Route::post('/project/{project}/milestone/{milestone}/complete' , [MilestoneController::class, 'completeMilestone']);
});

