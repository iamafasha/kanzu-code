<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    use HasFactory;


    protected $fillable = [
        'name',
        'description',
        'status',
    ];

    public function manager()
    {
        return $this->belongsTo(User::class,'managed_by');
    }

    public function developers()
    {
        return $this->belongsToMany(User::class, 'project_user', 'project_id', 'user_id');
    }


    public function milestones()
    {
        return $this->hasMany(Milestone::class);
    }



    public function projectEngineers()
    {
        return $this->hasMany(ProjectUser::class, 'project_id');
    }

}
