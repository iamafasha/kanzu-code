<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMilestoneRequest;
use App\Http\Requests\UpdateMilestoneRequest;
use App\Models\Milestone;
use App\Models\Project;

class MilestoneController extends Controller
{
    /**
     *
     * @group Milestone
     * Display a listing of the resource.
     */
    public function index()
    {
        $milestones = Milestone::all();
        return response()->json(['milestones' => $milestones], 200);
    }

    /**
     * @group Milestone
     * Store a newly created resource in storage.
     */
    public function store(StoreMilestoneRequest $request)
    {
        $validatedData = $request->validated();
        $milestone = Milestone::create($validatedData);
        return response()->json(['milestone' => $milestone], 201);
    }

    /**
     * @group Milestone
     * Display the specified resource.
     */
    public function show(Milestone $milestone)
    {
        return response()->json(['milestone' => $milestone], 200);
    }

    /**
     *
     * @group Milestone
     * Update the specified resource in storage.
     */
    public function update(UpdateMilestoneRequest $request, Milestone $milestone)
    {
        $validatedData = $request->validated();
        $milestone->update($validatedData);
        return response()->json(['project' => $milestone], 200);
    }

    /**
     *
     * @group Milestone
     * Remove the specified resource from storage.
     */
    public function destroy(Milestone $milestone)
    {
        $milestone->delete();
        return response()->json(['message' => 'MileStone deleted successfully'], 200);
    }

/**
 *
 *
* @group Milestone
 *
 * @param Project $project
 * @param Milestone $milestone
 * @return void
 */
    public function completeMilestone(Project $project, Milestone $milestone){
        $milestone->update([
          'status' => 'complete',
        ]);
        return response()->json(['milestone' => $milestone], 200);
    }
}
