<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Models\Project;
use App\Models\ProjectUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProjectController extends Controller
{
    /**
     *
     * @group Project
     * Display a listing of the resource.
     */
    public function index()
    {

        $projects = Project::all();
        return response()->json(['projects' => $projects], 200);
    }

    /**
     * @group Project
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request)
    {
        $validatedData = $request->validated();
        $project = Project::create($validatedData);

        $project->projectEngineers()->create([
            'user_id' => $validatedData['managed_by'],
            'is_pm' => true,
        ]);

        return response()->json(['project' => $project], 201);
    }

    /**
     *  @group Project
     * Display the specified resource.
     */
    public function show(Project $project)
    {
        return response()->json(['project' => $project], 200);
    }

    /**
     *   @group Project
     * Update the specified resource in storage.
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        $validatedData = $request->validated();
        $project->update($validatedData);
        return response()->json(['project' => $project], 200);
    }

    /**
     *   @group Project
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return response()->json(['message' => 'Project deleted successfully'], 200);
    }


    /**
     *   @group Project
     *
     * @param Project $project
     * @return void
     */
    public function completeProject(Project $project)
    {
        $project->update([
            'status' => 'complete',
        ]);
        return response()->json(['project' => $project], 200);
    }

    /**
     *
     *   @group Project
     *
     * @param Request $request
     * @param Project $project
     * @return void
     */
    public function addEngineer(Request $request, Project $project)
    {
        $validator = Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $existingEngineer = ProjectUser::where('user_id', $request->user_id)
                                            ->where('project_id', $project->id)
                                            ->first();

        if (!$existingEngineer) {
            $project->projectEngineers()->create([
                'user_id' => $request->user_id,
                'is_pm' => false,
            ]);

            return response()->json(['project' => $project], 200);
        }

        return response()->json(['error' => 'User already exists as an engineer in this project'], 422);
    }

    /**
     *   @group Project
     *
     * @param Project $project
     * @param User $user
     * @return void
     */
    public function removeEngineer(Project $project, User $user)
    {
        $projectEngineer = ProjectUser::where('project_id', $project->id)
                                           ->where('user_id', $user->id)
                                           ->first();

        if ($projectEngineer) {
            $projectEngineer->delete();
            return response()->json(['message' => 'Engineer removed successfully'], 200);
        }

        return response()->json(['error' => 'Engineer not found in this project'], 404);
    }
}
