<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     *
     * @group User
     * Display the specified resource.
     */
    public function show($id)
    {
        $user = User::with(['projects.milestones'])->findOrFail($id);
        return $user;
    }


}
