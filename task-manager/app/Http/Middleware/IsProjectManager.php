<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class IsProjectManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        //$request->user()->id

        if(isset($request->project)){
           $project = $request->project;

           $pm = $project->projectEngineers()
                ->where('user_id', $request->user()->id)
                ->where('is_pm', true)
                ->first();

            if($pm){
                return $next($request);
            }
        }

        return response()->json(['message' => 'You should be a project manager of this project to do this'], 403);
    }
}
