<?php

$paragraph = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

function extractEmails($text) {
    $words = explode(' ', $text);
    $emails = [];

    foreach ($words as $word) {
        if (strpos($word, '@') !== false) {
            $emails[] = $word;
        }
    }

    return $emails;
}

function extractPhoneNumbers($text) {
    $words = explode(' ', $text);
    $phoneNumbers = [];

    foreach ($words as $word) {
        if (strpos($word, '256') === 0) {
            $phoneNumbers[] = $word;
        }
    }

    return $phoneNumbers;
}

function extractUrls($text) {
    $words = explode(' ', $text);
    $urls = [];

    foreach ($words as $word) {
        if (strpos($word, 'https://') === 0 || strpos($word, 'http://') === 0) {
            $urls[] = $word;
        }
    }

    return $urls;
}

$emailAddresses = extractEmails($paragraph);
$phoneNumbers = extractPhoneNumbers($paragraph);
$urls = extractUrls($paragraph);

echo "Email Addresses: " . implode(", ", $emailAddresses) . "\n";
echo "Phone Numbers: " . implode(", ", $phoneNumbers) . "\n";
echo "URLs: " . implode(", ", $urls) . "\n";
