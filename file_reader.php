<?php

function getUniqueItemsFromFile($filename) {
    $fileContents = file_get_contents($filename);
    if ($fileContents === false) {
        return [];
    }
    $items = explode(" ", $fileContents); 
    $uniqueItems = array_values(array_unique($items));
    return $uniqueItems;
}


$filename = 'test-file.txt';
$uniqueItems = getUniqueItemsFromFile($filename);

print_r($uniqueItems);
